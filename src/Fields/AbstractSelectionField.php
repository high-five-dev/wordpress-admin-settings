<?php

/**
 * @package:    high-five-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@high-five.dev>
 * @copyright:  2020 - HighFive
 *
 * Created:     2020-04-01, 03:00:33 pm
 * Modified:    2020-10-17, 02:08:13 pm
 * Modified By: Harm Putman <harm@high-five.dev>
 */

namespace HighFive\WordPressAdminSettings\Fields;

defined('ABSPATH') or die('Shut the fuck up, Donny...');

abstract class AbstractSelectionField extends AbstractField
{
    protected $options;

    public function __construct($args)
    {
        parent::__construct($args);

        $this->options = isset($args['options']) ? $args['options'] : [];
    }

    public function getOptions()
    {
        return $this->options;
    }
}
