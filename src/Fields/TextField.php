<?php

/**
 * @package:    high-five-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@high-five.dev>
 * @copyright:  2020 - HighFive
 *
 * Created:     2020-03-30, 04:47:20 pm
 * Modified:    2020-10-17, 02:08:13 pm
 * Modified By: Harm Putman <harm@high-five.dev>
 */

namespace HighFive\WordPressAdminSettings\Fields;

defined('ABSPATH') or die('Shut the fuck up, Donny...');

final class TextField extends AbstractField
{
    protected function getHtml()
    {
        $field_classes = $this->getFieldClasses() ?: [ 'regular-text' ];
        ob_start(); ?>
<input
    type="<?php echo $this->getType(); ?>"
    class="<?php echo implode(' ', $field_classes); ?>"
    id="<?php echo $this->getID(); ?>"
    name="<?php echo $this->getName(); ?>"
    value="<?php echo $this->getValue(); ?>"
    placeholder="<?php echo $this->getPlaceholder(); ?>"
>
<?php echo $this->getDescriptionHTML(); ?>
<?php
        return ob_get_clean();
    }
}
