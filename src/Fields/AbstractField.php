<?php

/**
 * @package:    high-five-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@high-five.dev>
 * @copyright:  2020 - HighFive
 *
 * Created:     2020-03-30, 04:45:40 pm
 * Modified:    2020-10-17, 02:08:13 pm
 * Modified By: Harm Putman <harm@high-five.dev>
 */

namespace HighFive\WordPressAdminSettings\Fields;

defined('ABSPATH') or die('Shut the fuck up, Donny...');

abstract class AbstractField
{
    protected $option_name;

    protected $label_for;

    protected $value;

    protected $default;

    protected $name;

    protected $placeholder;

    protected $description;

    protected $field_classes;

    abstract protected function getHtml();

    public function __construct($args)
    {
        $this->type          = isset($args['type']) ? $args['type'] : '';
        $this->option_name   = isset($args['option_name']) ? $args['option_name'] : '';
        $this->label         = isset($args['label']) ? $args['label'] : '';
        $this->label_for     = isset($args['label_for']) ? $args['label_for'] : '';
        $this->value         = isset($args['value']) ? $args['value'] : '';
        $this->default       = isset($args['default']) ? $args['default'] : '';
        $this->name          = isset($args['name']) ? $args['name'] : '';
        $this->placeholder   = isset($args['placeholder']) ? $args['placeholder'] : '';
        $this->description   = isset($args['description']) ? $args['description'] : '';
        $this->field_classes = isset($args['field_classes']) ? $args['field_classes'] : [];
    }

    public function __toString()
    {
        return $this->getHtml();
    }

    protected function getType()
    {
        return $this->type;
    }

    protected function getLabel()
    {
        return $this->label;
    }

    protected function getID()
    {
        return $this->label_for;
    }

    protected function getName()
    {
        if (!$this->hasFieldArgs()) {
            return;
        }

        return $this->option_name . '[' . $this->getID() . ']';
    }

    protected function getValue()
    {
        $options = get_option($this->option_name);

        if (!$options || !isset($options[$this->getID()])) {
            return $this->getDefault();
        }

        return $options[$this->getID()];
    }

    protected function getDefault()
    {
        return $this->default;
    }

    protected function getPlaceholder()
    {
        return $this->placeholder;
    }

    protected function getDescriptionHTML()
    {
        if (!$this->description) {
            return false;
        }
        ob_start(); ?>
<p class="description"><?php echo esc_html($this->description); ?></p>
        <?php
        return ob_get_clean();
    }

    protected function getFieldClasses()
    {
        return $this->field_classes;
    }

    protected function hasFieldArgs()
    {
        return $this->option_name && $this->label_for;
    }
}
