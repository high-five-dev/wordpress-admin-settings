<?php

/**
 * @package:    high-five-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@high-five.dev>
 * @copyright:  2020 - HighFive
 *
 * Created:     2020-03-31, 04:08:12 pm
 * Modified:    2020-10-17, 02:08:13 pm
 * Modified By: Harm Putman <harm@high-five.dev>
 */

namespace HighFive\WordPressAdminSettings\Fields;

defined('ABSPATH') or die('Shut the fuck up, Donny...');

final class RadioField extends AbstractSelectionField
{
    protected function getHtml()
    {
        ob_start(); ?>
<fieldset>
    <legend class="screen-reader-text"><?php echo $this->getLabel(); ?></legend>
    <?php foreach ($this->getOptions() as $key => $value) : ?>
        <?php $checked = $key === $this->getValue() ? ' checked="checked"' : ''; ?>
        <label for="<?php echo $this->getChoiceID($key); ?>">
            <input
                type="radio"
                name="<?php echo $this->getName(); ?>"
                id="<?php echo $this->getChoiceID($key); ?>"
                value="<?php echo $key; ?>"
                <?php echo $checked; ?>
            >
            <?php echo $value; ?>
        </label>
        <br>
    <?php endforeach; ?>
</fieldset>
<?php echo $this->getDescriptionHTML(); ?>
<?php
        return ob_get_clean();
    }

    protected function getChoiceID($key)
    {
        return $this->getID() . '_' . $key;
    }
}
