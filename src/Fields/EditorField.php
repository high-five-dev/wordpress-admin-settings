<?php

/**
 * @package:    high-five-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@high-five.dev>
 * @copyright:  2020 - HighFive
 *
 * Created:     2020-04-08, 03:40:09 pm
 * Modified:    2020-10-17, 02:08:13 pm
 * Modified By: Harm Putman <harm@high-five.dev>
 */

namespace HighFive\WordPressAdminSettings\Fields;

defined('ABSPATH') or die('Shut the fuck up, Donny...');

final class EditorField extends AbstractField
{
    protected function getHtml()
    {
        $settings = [
            'media_buttons'    => false,
            'drag_drop_upload' => false,
            'textarea_rows'    => 4,
            'textarea_name'    => $this->getName(),
            'editor_class'     => implode(' ', $this->getFieldClasses()),
            'teeny'            => true,
        ];
        ob_start();
        wp_editor($this->getValue(), $this->getID(), $settings);
        echo $this->getDescriptionHTML();

        return ob_get_clean();
    }
}
