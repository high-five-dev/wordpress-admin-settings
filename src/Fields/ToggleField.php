<?php

/**
 * @package:    high-five-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@high-five.dev>
 * @copyright:  2020 - HighFive
 *
 * Created:     2020-03-31, 11:26:43 am
 * Modified:    2020-10-17, 02:08:13 pm
 * Modified By: Harm Putman <harm@high-five.dev>
 */

namespace HighFive\WordPressAdminSettings\Fields;

defined('ABSPATH') or die('Shut the fuck up, Donny...');

final class ToggleField extends AbstractField
{
    protected function getHtml()
    {
        ob_start(); ?>
<div class="high-five-wordpress-admin-settings-ui-toggle">
    <input
        type="checkbox"
        id="<?php echo $this->getID(); ?>"
        name="<?php echo $this->getName(); ?>"
        value="toggle_on"
        <?php echo $this->isChecked(); ?>
    >
    <label for="<?php echo $this->getID(); ?>"></label>
    <?php echo $this->getDescriptionHTML(); ?>
</div>
<?php
        return ob_get_clean();
    }

    protected function isChecked()
    {
        return $this->getValue() ? 'checked' : '';
    }
}
