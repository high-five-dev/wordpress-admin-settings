<?php

/**
 * @package:    high-five-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@high-five.dev>
 * @copyright:  2020 - HighFive
 *
 * Created:     2020-03-31, 04:08:12 pm
 * Modified:    2020-10-17, 02:08:13 pm
 * Modified By: Harm Putman <harm@high-five.dev>
 */

namespace HighFive\WordPressAdminSettings\Fields;

defined('ABSPATH') or die('Shut the fuck up, Donny...');

final class SelectField extends AbstractSelectionField
{
    protected function getHtml()
    {
        $field_classes = $this->getFieldClasses() ?: [];
        ob_start(); ?>
<select
    name="<?php echo $this->getName(); ?>"
    id="<?php echo $this->getID(); ?>"
    class="<?php echo implode(' ', $field_classes); ?>">
    <?php foreach ($this->getOptions() as $key => $value) : ?>
        <?php $selected = strval($key) === $this->getValue() ? ' selected="selected"' : ''; ?>
        <option value="<?php echo $key; ?>"<?php echo $selected; ?>><?php echo $value; ?></option>
    <?php endforeach; ?>
</select>
<?php echo $this->getDescriptionHTML(); ?>
<?php
        return ob_get_clean();
    }

    protected function getChoiceID($key)
    {
        return $this->getID() . '_' . $key;
    }
}
