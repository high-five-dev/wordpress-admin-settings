<?php

/**
 * @package:    high-five-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@high-five.dev>
 * @copyright:  2020 - HighFive
 *
 * Created:     2020-08-07, 04:45:26 pm
 * Modified:    2020-10-17, 02:08:13 pm
 * Modified By: Harm Putman <harm@high-five.dev>
 */

namespace HighFive\WordPressAdminSettings;

defined('ABSPATH') or die('Shut the fuck up, Donny...');

final class Callbacks
{
    public function sanitizeSettings($settings)
    {
        foreach ($settings as $field_id => $setting) {
            if ('toggle_on' === $setting) {
                $settings[$field_id] = true;
            }
        }

        return $settings;
    }

    public function settingsSectionCallback()
    {
        return;
    }

    public function settingsField($args = [])
    {
        $class = $this->getSettingsFieldClass($args['type']);

        echo new $class($args);
    }

    protected function getSettingsFieldClass($type = null)
    {
        $type = $type ?: 'text';

        if ( class_exists( $type ) ) {
            return $type;
        }

        $classname = ucfirst($type) . 'Field';
        $namespace = 'HighFive\\WordPressAdminSettings\\Fields\\';

        return class_exists($namespace . $classname) ? $namespace . $classname : $namespace . 'TextField';
    }
}
