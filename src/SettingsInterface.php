<?php

/**
 * @package:    high-five-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@high-five.dev>
 * @copyright:  2020 - HighFive
 *
 * Created:     2020-08-10, 08:51:51 am
 * Modified:    2020-10-17, 02:08:13 pm
 * Modified By: Harm Putman <harm@high-five.dev>
 */

namespace HighFive\WordPressAdminSettings;

defined('ABSPATH') or die('Shut the fuck up, Donny...');

interface SettingsInterface
{
    public function register();
    public function getSettingsID();
    public function addSettings($settings);
}
