<?php

/**
 * @package:    high-five-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@high-five.dev>
 * @copyright:  2020 - HighFive
 *
 * Created:     2020-07-08, 02:50:19 pm
 * Modified:    2020-10-17, 02:08:13 pm
 * Modified By: Harm Putman <harm@high-five.dev>
 */

namespace HighFive\WordPressAdminSettings;


defined('ABSPATH') or die('Shut the fuck up, Donny...');

abstract class AbstractSettings implements SettingsInterface
{
    protected $settings_id;

    public function __construct()
    {
        $this->settings_id = $this->getSettingsID();
    }

    public static function get($field_id, $default = null)
    {
        $instance = new static();

        $options = get_option($instance->settings_id);

        if (!$options || !isset($options[$field_id])) {
            return $default;
        }

        return $options[$field_id];
    }
}
